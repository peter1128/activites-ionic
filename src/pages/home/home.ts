import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { ActivitiesPage } from '../activities/activities';
import { RegisterPage } from '../register/register';
import { FirstPage } from '../firstpage/firstpage';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  username:string;
  password:string
  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {

  }

  login(){
    let user = "";
    let pw = "";
    if (user==this.username && pw == this.password || 1==1){
      //console.log("Username:123", this.username);
      //console.log("Password:", this.password);
      this.navCtrl.push(FirstPage);
    }
    else {
      const alert = this.alertCtrl.create({
        title: 'Can\'t Login',
        subTitle: 'Wrong Username OR Password',
        buttons: ['OK']
    });
      alert.present();}
  }
  goRegister(){
    this.navCtrl.push(RegisterPage);
  }

}
